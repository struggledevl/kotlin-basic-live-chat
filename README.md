## Fullscreen ExoPlayer

##### 1. Deklarasi variable untuk Fullscreen button dan fullscreen boolean, pada PlayerActivity
> Pada PlayerActivity ini kita mendeklarasikan variable fullScreenButton,
> dan fullscreen untuk menampung fullScreenButton ImageView, dan fullscreen boolean variable
> yang nantinya dipakai untuk menjalankan fungsi fullscreen dan normalscreen.
```
private var fullScreenButton: ImageView? = null
private var fullscreen = false
```

##### 2. Initialize variable pada onCreate untuk fullScreenButton
> Pada PlayerActivity membuat initialize untuk fullScreenButton yang disimpan di dalam fungsi override
> lifecycle onCreate yang berfungsi sebagai  wadah untuk menginitialize fungsi - fungsi yang ada pada class
> PlayerActivity
```
public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    fullScreenButton = findViewById(R.id.exo_fullscreen_btn_id)
    fullScreenButton?.setOnClickListener(this)
}
```

##### 3. Membuat fungsi onClick Method untuk menampung fullscreen condition
> Setelah itu membuat fungsi override onClick untuk menampung kondisi fullscreen, dan
> pada kondisi fullscreen dan normalscreen terdapat fungsi untuk hide and show supportActionBar,
> dan fungsi  untuk merubah drawable normalscreen dan fullscreen
```
// membuat fungsi override untuk onClick
    override fun onClick(view: View) {
        if (view === fullScreenButton) {
            if (fullscreen) {
                fullScreenButton!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_open)
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
                if (supportActionBar != null) {
                    supportActionBar!!.show()
                }
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                fullscreen = false
            } else {
                fullScreenButton!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_close)
                window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                )
                if (supportActionBar != null) {
                    supportActionBar!!.hide()
                }
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                fullscreen = true
            }
        }
    }
```

##### 4. Membuat ImageView pada layout exo_playback_control_view.xml
> Kemudian membuat ImageView didalam layout exo_playback_control_view sebagai wadah
> untuk menampung id exo_fullscreen_btn_id, yang nantinya ditampilkan pada ui
> dan juga sebagai wadah fungsi fullscreen itu sendiri
```
<ImageView
    android:id="@+id/exo_fullscreen_btn_id"
    android:layout_width="32dp"
    android:layout_height="32dp"
    android:layout_gravity="center"
    android:adjustViewBounds="true"
    android:scaleType="fitCenter"
    android:src="@drawable/ic_fullscreen_open" />
```

### Setting Method

##### 5. Membuat fungsi setting, deklarasi variable
> Pada PlayerActivity ini kita mendeklarasikan variable settingsButton, untuk menampung settingsButton ImageView
> yang nantinya dipakai untuk menjalankan fungsi setting untuk select video quality.
```
private var settingsButton: ImageView? = null
```

#

##### 6. Initialize variable dan setOnClickListener pada onCreate
> menginitialize variable settingsButton sebagai R.id.exo_settings_icon_id,
> dan initialize fungsi setOnClickListener pada fungsi override lifecycle onCreate()
```
public override fun onCreate(savedInstanceState: Bundle?) {
    settingsButton = findViewById(R.id.exo_settings_icon_id)
    settingsButton?.setOnClickListener(this)
}
```

##### 7. Membuat class TrackSelectionDialog

> Membuat class TrackSelectionDialog untuk menampung dialog selectTrackSelector 
> atau dialog setting pemilihan quality video, dan pada TrackSelectionDialog
> memuat fungsi - fungsi untuk membuat dialog Pemilihan video quality

##### TrackSelectionDialog :

> Mengembalikan nilai disabled
>
> @param rendererIndex Renderer index.
> @mengembalikan nilai jika renderer bernilai disabled
>
> [getIsDisabled ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L91)

##

> @param rendererIndex Renderer index.
> @Mengembalikan nilai list dari track selection
>  
> [getOverrides ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L103)

##

> Fungsi onCreateDialog, untuk membuat dialog
>
> [onCreateDialog ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L110)

##

> fungsi onDismiss dialog
>
> [onDismiss ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L120)

##

> Fungsi onCreateView pada fragment, sebagai wadah view pada fragment
>
> [onCreateView ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L126)

##

> Fragment to show a track selection in tab of the track selection dialog.
>
> [TrackSelectionViewFragment ( ) { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L167)

##

> method untuk menampung variable, value, dan fungsi
> untuk dipanggil kembali tanpa melalui object
>
>companion object {
>
> Mengembalikan nilai ketika track selection dialog memiliki content untuk di tampilkan,
> jika diinitialize dengan [MappedTrackInfo] pada fungsi ini
>
> [willHaveContent () {...}](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L233)
>

###

> Mengembalikan nilai ketika track selection dialog memiliki content untuk di tampilkan,
> jika initialize dengan [DefaultTrackSelector] pada fungsi class ini
>
> [willHaveContent () {...}](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L246)
>

###

> @param trackSelector dengan type [DefaultTrackSelector].
> @param onDismissListener dengan type [DialogInterface.OnDismissListener] dipanggil ketika
> dialog dismissed.
>
>[createForTrackSelector () {...}](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L256) 
>

###

> Fungsi untuk memanggil Tabbing
>
>[showTabForRenderer () {...}](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L311)
>

###

> Fungsi untuk memanggil tipe track video, audio, atau text
>
>[getTrackTypeString () {...}](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L327)
>
>}

##

> fungsi init
>
> [init { ... }](https://gitlab.com/struggledevl/kotlin-basic-live-chat/-/blob/master/app/src/main/java/com/drt/rctiplus/TrackSelectionDialog.kt#L338)


##### 8. Membuat kondisi settings pada onClick Method
> Membuat kondisi if pada fungsi override onClick yang akan mentrigger dialog
> untuk memilih quality video / track selector video, jika view / tapping pada settingsButton,
> isShowingTrackSelectionDialog false, dan **_TrackSelectionDialog willHaveContent_**,
> maka TrackSelectionDialog akan menampilkan video quality dari fungsi _**TrackSelectionDialog.createForTrackSelector**_
```
    override fun onClick(view: View) {
        // setting or select track button condition
        if (view === settingsButton && !isShowingTrackSelectionDialog && trackSelector?.let { TrackSelectionDialog.willHaveContent(it) }!!) {
            isShowingTrackSelectionDialog = true
            trackSelector?.let {
                TrackSelectionDialog.createForTrackSelector(
                    it  /* onDismissListener= */
                ) {
                    isShowingTrackSelectionDialog = false
                }
            }?.show(supportFragmentManager,  /* tag= */null)
        }
    }
```

##### 9. Membuat ImageView pada layout exo_playback_control_view.xml
> Kemudian membuat ImageView didalam layout exo_playback_control_view sebagai wadah
> untuk menampung id exo_settings_icon_id, yang nantinya ditampilkan pada ui
> dan juga sebagai wadah fungsi setting itu sendiri
```
<ImageView
    android:id="@+id/exo_settings_icon_id"
    android:layout_width="24dp"
    android:layout_height="24dp"
    android:layout_gravity="center"
    android:layout_marginHorizontal="20dp"
    android:adjustViewBounds="true"
    android:scaleType="fitCenter"
    android:src="@drawable/ic_settings" />
```