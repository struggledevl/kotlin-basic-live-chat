/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drt.rctiplus

import android.app.Application
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.*
import com.google.android.exoplayer2.util.Log
import com.google.android.exoplayer2.util.Util
import java.io.File

/**
 * class untuk menampung fungsi [buildDataSourceFactory], [buildHttpDataSourceFactory],
 * dan companion object yang berisi fungsi [buildReadOnlyCacheDataSource]
 */

// class DemoApplication
open class DemoApplication : Application() {

    // deklarasi variable userAgent type string bernilai null
    private var userAgent: String? = null

    // deklarasi variable databaseProvider type DatabaseProvider bernilai null
    private var databaseProvider: DatabaseProvider? = null
        // Metode get () dari antarmuka Daftar mengembalikan elemen pada posisi yang ditentukan dalam daftar ini.
        // metode get untuk mendapatkan ExoDatabaseProvider
        get() {
            if (field == null) {
                field = ExoDatabaseProvider(this)
            }
            return field
        }

    // deklarasi variable downloadDirectory type File
    private var downloadDirectory: File? = null
        // metode get untuk mendapatkan filesDir
        get() {
            if (field == null) {
                field = getExternalFilesDir(null)
                if (field == null) {
                    field = filesDir
                }
            }
            return field
        }

    @get:Synchronized
    // deklarasi variable downloadCache
    protected var downloadCache: Cache? = null
        // fungsi get field
        get() {
            if (field == null) {
                val downloadContentDirectory = File(downloadDirectory, DOWNLOAD_CONTENT_DIRECTORY)
                field = SimpleCache(downloadContentDirectory, NoOpCacheEvictor(), databaseProvider)
            }
            Log.d("field", "$field")
            return field
        }
        private set

    // fungsi onCreate
    override fun onCreate() {
        super.onCreate()

        // memasukkan value kedalam userAgent
        userAgent = Util.getUserAgent(this, "ExoPlayerDemo")
        Log.d("userAgent", "" + userAgent)
    }

    /**
     * Returns a [DataSource.Factory].
     */
    fun buildDataSourceFactory(): DataSource.Factory {
        val upstreamFactory = DefaultDataSourceFactory(this, buildHttpDataSourceFactory())
        return buildReadOnlyCacheDataSource(upstreamFactory, downloadCache)
    }

    /**
     * Returns a [HttpDataSource.Factory].
     **/
    private fun buildHttpDataSourceFactory(): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(userAgent)
    }

    /**
     * https://kotlinlang.org/docs/tutorials/kotlin-for-py/objects-and-companion-objects.html#companion-objects
     *
     * If you need a function or a property to be tied to a class rather than to instances of it (similar to @staticmethod in Python),
     * you can declare it inside a companion object:
     */
    companion object {
        private const val DOWNLOAD_CONTENT_DIRECTORY = "downloads"
        protected fun buildReadOnlyCacheDataSource(
                upstreamFactory: DataSource.Factory?, cache: Cache?): CacheDataSourceFactory {
            return CacheDataSourceFactory(
                    cache,
                    upstreamFactory,
                    FileDataSource.Factory(),
                    /* cacheWriteDataSinkFactory= */
                    null,
                    CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                    /* eventListener= */
                    null
            )
        }
    }
}