package com.drt.rctiplus

import com.drt.rctiplus.adapters.MessageAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_chat.*
import com.drt.rctiplus.models.Message
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatActivity : AppCompatActivity() {

    // deklarasi variable databaseReference
    private var databaseReference: DatabaseReference? = null

    // onCreate lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** hide the title bar */
        supportActionBar?.hide()

        /** fungsi wadah untuk ui activitiy_chat */
        setContentView(R.layout.activity_chat)

        /** initialize function */
        initFirebase()
        setupSendButton()
        createFirebaseListener()

        /** deklarasi value intent */
        val intent = Intent(this, PlayerActivity::class.java)

        /** fungsi setOnClickListener untuk buttonExoPlayer */
        buttonExoPlayer.setOnClickListener {
            // start your next activity
            startActivity(intent)
        }
    }

    /**
     * fungsi private initFirebase
     * Setup firebase function
     */
    private fun initFirebase() {

        // firebase initializeapp function
        FirebaseApp.initializeApp(applicationContext)

        // firebaseDatabase getInstance function
        FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG)

        /**
         * get reference to our db
         * mendefinisikan nilai dari variable databaseReference
         * sebagai FirebaseDatabase.getInstance().reference
         **/
        databaseReference = FirebaseDatabase.getInstance().reference
    }

    /** Untuk membaca data di suatu lokasi dan memproses perubahan,
     * menggunakan metode addValueEventListener()
     * untuk menambahkan ValueEventListener ke DatabaseReference
     * */
    private fun createFirebaseListener() {

        /** public interface ValueEventListener
         * Classes implementing this interface can be used to receive events about data changes at a location.
         * Attach the listener to a location user
         *
         * Class yang mengimplementasikan interface ini dapat digunakan untuk menerima events tentang perubahan data di suatu lokasi.
         * dan melampirkan listener ke lokasi user
         *
         * Public Method Summary / method yang digunakan
         * onCancelled(DatabaseError error) , dan onDataChange(DataSnapshot snapshot) **/
        val postListener = object : ValueEventListener {

            /** Metode onDataChange() Metode ini akan dipanggil bersama DataSnapshot pada fungsi ini.
             * dan juga akan dipanggil setiap kali data berubah. */
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                /** deklarasi variable arrayMessage berupa ArrayList dengan type ArrayList<Message> */
                val arrayMessage: ArrayList<Message> = ArrayList()

                /** looping sebanyak jumlah data berupa DataSnapshot dari dataSnapshot.children,
                 * untuk menampilkan view list data message berupa arrayList */
                for (data in dataSnapshot.children) {

                    /** deklarasi value messageData dengan type Message yang berisi nilai
                     * data.getValue<Message>(Message::class.java) */
                    val messageData = data.getValue<Message>(Message::class.java)

                    /** deklarasi value message dengan type Message yang berisi nilai
                     * messageData?.run { this } ?: continue */
                    val message = messageData?.run { this } ?: continue

                    /** memasukkan atau add nilai message kedalam arrayMessage */
                    arrayMessage.add(message)

                    println("dataSnapshot.children =============================: ${dataSnapshot.children}")
                }

                /** sort so newest at bottom
                 * mensorting message data dengan system timestamp */
                arrayMessage.sortBy {
                    it.timestamp
                }

                /** call function setupAdapter(arrayMessage)
                 * memanggil fungsi setupAdapter(arrayMessage),
                 * untuk menampilkan data list setupAdapter */
                setupAdapter(arrayMessage)
            }

            override fun onCancelled(error: DatabaseError) {
                println("show console error")
            }
        }

        /** ?. memanggil metode atau mengakses properti jika penerima tidak null
         *  databaseReference mengakses properti child "messages_from_chatFirebase",
         *  dan mengakses method addvalueEventListener(postListener)
         *  addValueEventListener yaitu, Untuk membaca data di suatu lokasi dan memproses perubahan data tsb */
        databaseReference?.child("messages_from_chatFirebase")?.addValueEventListener(postListener)
    }

    /** Once data is here - display it
     * fungsi untuk menampilkan data widget RecycleView pada activity_main.xml */
    @SuppressLint("ShowToast")
    private fun setupAdapter(data: ArrayList<Message>) {

        /** deklarasi variable linearLayoutManager dengan type LinearLayoutManager */
        val linearLayoutManager = LinearLayoutManager(this)

        /** mendifinisikan mainActivityRecyclerView.layoutManager sebagai linearLayoutManager */
        mainActivityRecyclerView.layoutManager = linearLayoutManager

        /** mendifinisikan mainActivityRecyclerView.adapter sebagai MessageAdapter(data),
         * yang berisi data dengan type ArrayList<Message> yang berisi arrayList dari Message */
        mainActivityRecyclerView.adapter = MessageAdapter(data) {
            Toast.makeText(this, "${it.text} clicked", Toast.LENGTH_SHORT)
            hideKeyboard()
        }

        /** scroll to bottom, mainActivityRecyclerView scroll posisi data / textnya selalu pada bagian bawah atau terakhir */
        mainActivityRecyclerView.scrollToPosition(data.size - 1)
    }

    /** hide and show dismiss keyboard
     * fungsi hide and show keyboard */
    private fun hideKeyboard() {
        /** deklarasi value inputManager dan inisialisasi nilai */
        val inputManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
                currentFocus?.windowToken,
                InputMethodManager.SHOW_FORCED
        )
    }

    /** fungsi method setOnClickListener untuk setup send button */
    private fun setupSendButton() {

        /** fungsi private untuk setup send button,
         * dengan kondisi if else didalamnya */

        /** yaitu membuat fungsi mainActivitySendButton.setOnClickListener,
         * setOnClickListener yaitu fungsi untuk button jika di button di click, atau touch
         * maka menjalankan kondisi if else */
        mainActivitySendButton.setOnClickListener {
            if (mainActivityEditText.text.toString().isEmpty()) {

                /** jika id mainActivityEditText pada ediText tidak diisi atau dalam keadaan null,
                mana melakukan Toast.makeText() dengan text toast warning atau "Please enter a message" */

                /** Toast condition if message empty */
                Toast.makeText(this, "Please enter a message", Toast.LENGTH_SHORT).show()
            } else {
                /** jika text tidak kosong, maka menjalankan fungsi sendData() */
                /** if text is not empty can send data **/
                sendData()
            }
        }
    }

    /**
     * fungsi untuk send data ke firebase
     * function for Send data to firebase database
     **/
    @SuppressLint("SimpleDateFormat")
    private fun sendData() {

        println("send data")

        /** deklarasi value dan type dari fungsi sendData */
        val timeString: Long = System.currentTimeMillis()

        /** deklarasi value simpeDateFormat dengan type SimpleDateFormat yang bernilai SimpleDateFormat("hh:mm:ss") */
        SimpleDateFormat("hh:mm:ss")

        /** deklarasi value date bertype Date dengan nilai Date(timeString) */
        val date = Date(timeString)

        /** membuat child databaseReference dan setValue berupa text */
        databaseReference
                ?.child("messages_from_chatFirebase")             // child pertama pada firebase
                ?.child(date.toString())                                    // child kedua pada firebase database
                ?.setValue(Message(mainActivityEditText.text.toString()))   // child ke-3 pada firebase database (isi dari Message com.drt.rctiplus.models)

        /** clear the text */
        mainActivityEditText.setText("")

        /** dismiss keyboard */
        hideKeyboard()
    }
}