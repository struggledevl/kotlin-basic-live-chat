/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.drt.rctiplus

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Pair
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException
import com.google.android.exoplayer2.source.BehindLiveWindowException
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.ParametersBuilder
import com.google.android.exoplayer2.trackselection.RandomTrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.ui.PlayerControlView
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.util.ErrorMessageProvider
import com.google.android.exoplayer2.util.EventLogger
import com.google.android.exoplayer2.util.Util
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy

/**
 * An activity that plays media using [SimpleExoPlayer].
 */
class PlayerActivity : AppCompatActivity(), View.OnClickListener, PlaybackPreparer, PlayerControlView.VisibilityListener {

    /**
     * Declaration variable and value on companion object
     */

    var settingsButton: ImageView? = null
    var fullScreenButton: ImageView? = null

    /* current time log & function variables */
    var lastValue: Long = 0
    var currentTime: Long = 0
    var multipleTen: Long = 0
    var zero: Long = 0

    // start time var param
    var paramStartTime: Long = 0

    /* Handler */
    val mHandler = Handler()

    /* Int & Long var */
    var startPosition: Long = 0
    var startWindow = 0

    /* boolean */
    var isFullscreen = false
    var isShowingTrackSelectionDialog = false
    var isStartAutoPlay = false

    /* Layout */
    var backButton: ImageView? = null            /* ImageView */
    var debugRootView: LinearLayout? = null      /* LinearLayout */
    var mTextView: TextView? = null              /* TextView */

    /* Exoplayer */
    var playerView: PlayerView? = null                                      /* PlayerView */
    var player: SimpleExoPlayer? = null                                     /* SimpleExoPlayer */
    var dataSourceFactory: DataSource.Factory? = null                       /* DataSource.Factory */
    var trackSelector: DefaultTrackSelector? = null                         /* DefaultTrackSelector */
    var lastSeenTrackGroupArray: TrackGroupArray? = null                    /* TrackGroupArray */
    var trackSelectorParameters: DefaultTrackSelector.Parameters? = null    /* DefaultTrackSelector.Parameters */

    companion object {

        const val ABR_ALGORITHM_RANDOM = "random"

        /* Activity extras */
        const val SPHERICAL_STEREO_MODE_EXTRA = "spherical_stereo_mode"

        /* Player configuration extras */
        const val ABR_ALGORITHM_EXTRA = "abr_algorithm"
        const val ABR_ALGORITHM_DEFAULT = "default"

        /* Media item configuration extras */
        const val TUNNELING_EXTRA = "tunneling"

        /* Saved instance state keys */
        const val KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters"
        const val KEY_WINDOW = "window"
        const val KEY_POSITION = "position"
        const val KEY_AUTO_PLAY = "auto_play"
        var DEFAULT_COOKIE_MANAGER: CookieManager? = null
    }

    /* onSaveInstanceState */
    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters)
        outState.putBoolean(KEY_AUTO_PLAY, isStartAutoPlay)
        outState.putInt(KEY_WINDOW, startWindow)
        outState.putLong(KEY_POSITION, startPosition)
    }

    /* onCreate */
    @SuppressLint("CutPasteId")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val sphericalStereoMode = intent.getStringExtra(SPHERICAL_STEREO_MODE_EXTRA)
        if (sphericalStereoMode != null) {
            setTheme(R.style.PlayerTheme_Spherical)
        }

        player?.playWhenReady = true

        dataSourceFactory = buildDataSourceFactory()
        if (CookieHandler.getDefault() !== DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER)
        }
        setContentView(R.layout.player_activity)

        /* initialize playerView */
        playerView = findViewById(R.id.player_view)
        playerView?.setControllerVisibilityListener(this)

        mTextView = findViewById(R.id.exo_position)

        /* initialize settings button */
        settingsButton = findViewById(R.id.exo_settings_icon_id)
        settingsButton?.setOnClickListener(this)

        /* initialize fullscreen button */
        fullScreenButton = findViewById(R.id.exo_fullscreen_btn_id)
        fullScreenButton?.setOnClickListener(this)

        /* initialize back button */
        backButton = findViewById(R.id.exo_back_icon_id)
        backButton?.setOnClickListener(this)

        /* initialize normal button */
        val normalScreenButton = findViewById<ImageView>(R.id.exo_fullscreen_btn_id)
        normalScreenButton.setOnClickListener(this)
        debugRootView = findViewById(R.id.controls_root)
        playerView?.setErrorMessageProvider(PlayerErrorMessageProvider())
        playerView?.requestFocus()
        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS)
            isStartAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY)
            startWindow = savedInstanceState.getInt(KEY_WINDOW)
            startPosition = savedInstanceState.getLong(KEY_POSITION)
            Log.e("restoring", "if onCreate() Restoring previous state")
        } else {
            val builder = ParametersBuilder( /* context= */this)
            val tunneling = intent.getBooleanExtra(TUNNELING_EXTRA, false)
            if (Util.SDK_INT >= 21 && tunneling) {
                builder.setTunnelingAudioSessionId(C.generateAudioSessionIdV21( /* context= */this))
            }
            trackSelectorParameters = builder.build()
            clearStartPosition()
            Log.e("restoring", "else onCreate() Restoring previous state")
        }
    }

    /* initialize */
    private fun initializePlayer() {
        if (player == null) {
            val intent = intent
            // trackselection
            val trackSelectionFactory: TrackSelection.Factory
            val abrAlgorithm = intent.getStringExtra(ABR_ALGORITHM_EXTRA)
            trackSelectionFactory = if (abrAlgorithm == null || ABR_ALGORITHM_DEFAULT == abrAlgorithm) {
                AdaptiveTrackSelection.Factory()
            } else if (Companion.ABR_ALGORITHM_RANDOM == abrAlgorithm) {
                RandomTrackSelection.Factory()
            } else {
                showToast(R.string.error_unrecognized_abr_algorithm)
                finish()
                return
            }

            // membuat trackselector
            trackSelector = DefaultTrackSelector( /* context= */this, trackSelectionFactory)
            trackSelector!!.parameters = trackSelectorParameters!!
            lastSeenTrackGroupArray = null

            // Creating the player, membuat player dengan fungsi SimpleExoPlayer.Builder
            player = SimpleExoPlayer.Builder( /* context= */this)
                    .setTrackSelector(trackSelector!!)
                    .build()

            // Log.d("trackSelector", "$trackSelector")
            player!!.addListener(PlayerEventListener())
            player!!.setAudioAttributes(AudioAttributes.DEFAULT,  /* handleAudioFocus= */true)
            player!!.playWhenReady = isStartAutoPlay
            player!!.addAnalyticsListener(EventLogger(trackSelector))
            playerView!!.setPlaybackPreparer(this)
            playerView!!.player = player
        }

        // parameter start time 1 minute or 60000 miliseconds
        paramStartTime = 60000

        // deklarasi variable haveStartPosition type boolean
        val haveStartPosition = startWindow != C.INDEX_UNSET

        val videoSource: MediaSource = HlsMediaSource.Factory(dataSourceFactory!!)
                .createMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"))

        if (haveStartPosition) {
            Log.e("haveStart", "have start position")
            player!!.seekTo(startWindow, startPosition)
        }
        player!!.prepare(videoSource)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        clearStartPosition()
        setIntent(intent)
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
            // seekTo fun for seek time programmatically
            player?.seekTo(paramStartTime)
            if (playerView != null) {
                playerView!!.onResume()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            if (playerView != null) {
                playerView!!.onPause()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer()
            if (playerView != null) {
                playerView!!.onResume()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            playerView!!.onPause()
        }
    }

    init {
        DEFAULT_COOKIE_MANAGER = CookieManager()
        DEFAULT_COOKIE_MANAGER!!.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)
    }

    /**
     * log Runnable for run updateProgress
     */
    private val fRunnable: Runnable = object : Runnable {
        override fun run() {
            mHandler.postDelayed(this, 1000)
            updateProgress()
        }
    }

    /* progress for runnable contentPosition check */
    private fun updateProgress() {
        currentTime = player?.contentPosition?.div(1000L)!!
        multipleTen = currentTime % 10L
        zero = 0L

        /* every 10 seconds condition */
        if (multipleTen != zero) {
            Log.d("updateProgress", "currentTime : $currentTime")
            Log.d("updateProgress", "lastValue : $lastValue")
        } else if ((multipleTen == zero) && (currentTime != zero) && (lastValue != currentTime)) {
            /* save value currentTime to lastValue */
            lastValue = currentTime;
            Log.e("updateProgress", "currentTime : $currentTime")
            currentTimeDialog()
        }
    }

    /* every 10 seconds current time dialog function */
    private fun currentTimeDialog() {
        val builder = AlertDialog.Builder(this@PlayerActivity, AlertDialog.THEME_DEVICE_DEFAULT_DARK)
        builder.setTitle("10 Seconds Passed...")
        builder.setMessage("Do you want really exit")
        builder.setCancelable(false)
        builder.setPositiveButton("Yes") { dialog, _ -> dialog.dismiss() }
        builder.setNegativeButton("No") { dialog, _ -> dialog.dismiss() }
        builder.create().show()
    }

    private fun isBehindLiveWindow(e: ExoPlaybackException): Boolean {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false
        }
        var cause: Throwable? = e.sourceException
        while (cause != null) {
            if (cause is BehindLiveWindowException) {
                return true
            }
            cause = cause.cause
        }
        return false
    }

    private inner class PlayerEventListener : Player.EventListener {

        /* onplayer error */
        override fun onPlayerError(e: ExoPlaybackException) {
            if (isBehindLiveWindow(e)) {
                initializePlayer()
                clearStartPosition()
            }
        }

        /* onplayer state changed */
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            if (playWhenReady && playbackState == Player.STATE_READY) {
                // media actually playing
                Log.d("updateProgress", "playing")
                mHandler.post(fRunnable)
            } else if (playWhenReady) {
                // might be idle (plays after prepare()),
                // or ended (plays when seek away from end)
                Log.d("updateProgress", "buffering")
                mHandler.removeCallbacks(fRunnable)
            } else {
                // player paused in any state
                Log.d("updateProgress", "pause")
                mHandler.removeCallbacks(fRunnable)
            }
            updateButtonVisibility()
        }
    }

    /* onClick method */
    override fun onClick(view: View) {

        // setting or select track button condition
        if (view === settingsButton && !isShowingTrackSelectionDialog && trackSelector?.let {
                    TrackSelectionDialog.willHaveContent(it)
                }!!) {
            isShowingTrackSelectionDialog = true
            trackSelector?.let {
                TrackSelectionDialog.createForTrackSelector(
                        it  /* onDismissListener= */
                ) {
                    isShowingTrackSelectionDialog = false
                }
            }?.show(supportFragmentManager,  /* tag= */null)
            Log.e("test", "test start position")
        }

        // kondisi untuk fullscreen dan normal screen
        if (view === fullScreenButton) {
            Log.e("test", "test start position")
            if (isFullscreen) {
                // change imageDrawable to ic_fullscreen_open
                fullScreenButton!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_open))
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE

                // show support action bar
                if (supportActionBar != null) {
                    supportActionBar!!.show()
                }

                // fungsi untuk merubah orientasi screen ke dalam keadaan portrait
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

                // merubah nilai fullscreen ke false
                isFullscreen = false

            } else {    // kondisi fullScreen

                // change imageDrawable to ic_fullscreen_close
                fullScreenButton!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_close))

                // fungsi untuk hide support action bar
                if (supportActionBar != null) {
                    supportActionBar!!.hide()
                }

                // fungsi untuk merubah kedalam keadaan fullscreen
                window.decorView.systemUiVisibility = (
                        View.SYSTEM_UI_FLAG_FULLSCREEN
                                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        )

                // fungsi untuk merubah orientasi screen aplikasi ke mode landscape
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

                // merubah nilai fullscreen ke true
                isFullscreen = true
            }
        }

        // kondisi back button
        if (view === backButton) {
            super.onBackPressed()
        }
    }

    override fun preparePlayback() {
        player!!.retry()
    }

    override fun onVisibilityChange(visibility: Int) {
        debugRootView!!.visibility = visibility
    }

    /**
     * Return a new DataSource factory.
     */
    private fun buildDataSourceFactory(): DataSource.Factory {
        // fungsi buildDataSourceFactory dari class DemoApplication
        return (application as DemoApplication).buildDataSourceFactory()
    }

    private fun clearStartPosition() {
        startPosition = C.TIME_UNSET
        startWindow = C.INDEX_UNSET
        isStartAutoPlay = true
    }

    /* toast */
    private fun showToast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }

    private fun showToast(messageId: Int) {
        showToast(getString(messageId))
    }

    /* button visible */
    private fun updateButtonVisibility() {
        settingsButton?.isEnabled = player != null && TrackSelectionDialog.willHaveContent(trackSelector!!)
    }

    /* show controls player */
    private fun showControls() {
        debugRootView!!.visibility = View.VISIBLE
    }

    private inner class PlayerErrorMessageProvider : ErrorMessageProvider<ExoPlaybackException> {
        override fun getErrorMessage(e: ExoPlaybackException): Pair<Int, String> {
            var errorString = getString(R.string.error_generic)
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                val cause = e.rendererException
                if (cause is DecoderInitializationException) {
                    // Special case for decoder initialization failures.
                    errorString = if (cause.codecInfo == null) {
                        when {
                            cause.cause is DecoderQueryException -> {
                                getString(R.string.error_querying_decoders)
                            }
                            cause.secureDecoderRequired -> {
                                getString(R.string.error_no_secure_decoder, cause.mimeType)
                            }
                            else -> {
                                getString(R.string.error_no_decoder, cause.mimeType)
                            }
                        }
                    } else {
                        getString(R.string.error_instantiating_decoder, cause.codecInfo!!.name)
                    }
                }
            }
            return Pair.create(0, errorString)
        }
    }
}