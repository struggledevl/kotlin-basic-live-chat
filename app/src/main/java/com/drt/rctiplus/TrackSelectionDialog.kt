/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.drt.rctiplus

import android.app.Dialog
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo
import com.google.android.exoplayer2.ui.TrackSelectionView
import com.google.android.exoplayer2.ui.TrackSelectionView.TrackSelectionListener
import com.google.android.exoplayer2.util.Assertions
import com.google.android.material.tabs.TabLayout
import java.util.*

/**
 * Dialog to select tracks.
 **/
@Suppress("SameParameterValue")

/**
 * 1. Membuat class TrackSelectionDialog extends DialogFragment
 **/
class TrackSelectionDialog : DialogFragment() {

    /**
     * 2. Membuat deklarasi variable, pada class ini
     **/
    private var titleId = 0
    private val tabTrackTypes: ArrayList<Int> = ArrayList()
    private var onClickListener: DialogInterface.OnClickListener? = null
    private var onDismissListener: DialogInterface.OnDismissListener? = null

    /**
     * 3. Membuat tabFragments
     **/
    private val tabFragments: SparseArray<TrackSelectionViewFragment> = SparseArray()

    /** 4. Membuat class TrackSelectionViewFragment
     * Fragment untuk menampilkan track selection pada tab track selection dialog */
    class TrackSelectionViewFragment : Fragment(), TrackSelectionListener {
        private var mappedTrackInfo: MappedTrackInfo? = null
        private var rendererIndex = 0
        private var allowAdaptiveSelections = false
        private var allowMultipleOverrides = false

        /* package */
        var isDisabled = false

        /* package */
        var overrides: List<SelectionOverride>? = null

        fun init(                                       /* init fun */
                mappedTrackInfo: MappedTrackInfo?,      /* mappedTrackInfo */
                rendererIndex: Int,                     /* rendererIndex */
                initialIsDisabled: Boolean,             /* initialIsDisabled */
                initialOverride: SelectionOverride?,    /* initialOverride */
                allowAdaptiveSelections: Boolean,       /* allowAdaptiveSelections */
                allowMultipleOverrides: Boolean         /* allowMultipleOverrides */
        ) {
            this.mappedTrackInfo = mappedTrackInfo
            this.rendererIndex = rendererIndex
            isDisabled = initialIsDisabled
            overrides = initialOverride?.let {
                listOf(it)
            } ?: emptyList()
            this.allowAdaptiveSelections = allowAdaptiveSelections
            this.allowMultipleOverrides = allowMultipleOverrides

            Log.d("overrides", "$overrides")
            Log.d("mappedTrackInfo", "$mappedTrackInfo")
        }

        // fungsi override onCreateView
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.exo_track_selection_dialog, container,  /* attachToRoot= */false)
            val trackSelectionView: TrackSelectionView = rootView.findViewById(R.id.exo_track_selection_view)
            trackSelectionView.setShowDisableOption(true)
            trackSelectionView.setAllowMultipleOverrides(allowMultipleOverrides)
            trackSelectionView.setAllowAdaptiveSelections(allowAdaptiveSelections)
            trackSelectionView.init(
                    mappedTrackInfo!!,
                    rendererIndex,
                    isDisabled,
                    overrides!!,
                    this    /* listener= */
            )
            return rootView
        }

        // fungsi override onTrackSelectionChanged
        override fun onTrackSelectionChanged(isDisabled: Boolean, overrides: List<SelectionOverride>) {
            this.isDisabled = isDisabled
            this.overrides = overrides
        }

        // fungsi init
        init {
            // Retain instance across activity re-creation to prevent losing access to init data.
            retainInstance = true
        }
    }

    /**
     * 5. Membuat fungsi init
     * */
    private fun init(                                               /* init functions */
            titleId: Int,                                           /* titleId */
            mappedTrackInfo: MappedTrackInfo,                       /* mappedTrackInfo */
            initialParameters: DefaultTrackSelector.Parameters,     /* initialParameters */
            setAllowAdaptiveSelections: Boolean,                    /* allowAdaptiveSelections */
            setAllowMultipleOverrides: Boolean,                     /* allowMultipleOverrides */
            onClickListener: DialogInterface.OnClickListener,       /* onClickListener */
            onDismissListener: DialogInterface.OnDismissListener    /* onDismissListener */
    ) {
        this.titleId = titleId
        this.onClickListener = onClickListener
        this.onDismissListener = onDismissListener

        for (i in 0 until mappedTrackInfo.rendererCount) {
            if (showTabForRenderer(mappedTrackInfo, i)) {
                val trackType = mappedTrackInfo.getRendererType( /* rendererIndex= */i)
                val trackGroupArray = mappedTrackInfo.getTrackGroups(i)
                val tabFragment = TrackSelectionViewFragment()
                tabFragment.init(
                        mappedTrackInfo,    /* rendererIndex= */
                        i,                  /* index */
                        initialParameters.getRendererDisabled( /* rendererIndex= */i),
                        initialParameters.getSelectionOverride( /* rendererIndex= */i, trackGroupArray),
                        setAllowAdaptiveSelections,
                        setAllowMultipleOverrides
                )
                tabTrackTypes.add(trackType)
                tabFragments.put(i, tabFragment)
            }
        }
    }

    /**
     * 12. Membuat companion object
     *
     * method untuk menampung variable, value, dan fungsi
     * untuk dipanggil kembali tanpa melalui object
     **/
    companion object {

        /**
         * Mengembalikan nilai ketika track selection dialog memiliki content untuk di tampilkan,
         * jika initialize dengan [DefaultTrackSelector] pada fungsi class ini
         */
        fun willHaveContent(trackSelector: DefaultTrackSelector): Boolean {
            val mappedTrackInfo = trackSelector.currentMappedTrackInfo
            return mappedTrackInfo != null && willHaveContent(mappedTrackInfo)
        }

        /**
         * Mengembalikan nilai ketika track selection dialog memiliki content untuk di tampilkan,
         * jika initialize dengan [MappedTrackInfo] pada fungsi class ini
         */
        private fun willHaveContent(mappedTrackInfo: MappedTrackInfo): Boolean {
            for (i in 0 until mappedTrackInfo.rendererCount) {
                if (showTabForRenderer(mappedTrackInfo, i)) {
                    return true
                }
            }
            return false
        }

        /**
         * fungsi showTabForRenderer
         * */
        private fun showTabForRenderer(mappedTrackInfo: MappedTrackInfo, rendererIndex: Int): Boolean {
            val trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex)
            if (trackGroupArray.length == 0) {
                return false
            }
            val trackType = mappedTrackInfo.getRendererType(rendererIndex)
            return isSupportedTrackType(trackType)
        }

        /**
         * 10. Membuat fungsi [isSupportedTrackType]
         * */
        private fun isSupportedTrackType(trackType: Int): Boolean {
            return when (trackType) {
                C.TRACK_TYPE_VIDEO, C.TRACK_TYPE_AUDIO, C.TRACK_TYPE_TEXT -> true
                else -> false
            }
        }

        /**
         * 15. Membuat [createForTrackSelector]
         *
         * @param trackSelector dengan type [DefaultTrackSelector].
         * @param onDismissListener dengan type [DialogInterface.OnDismissListener] dipanggil ketika
         * dialog dismissed.
         */
        fun createForTrackSelector(
                trackSelector: DefaultTrackSelector,
                onDismissListener: DialogInterface.OnDismissListener
        ): TrackSelectionDialog {

            // value mappedTrackInfo
            val mappedTrackInfo = Assertions.checkNotNull(trackSelector.currentMappedTrackInfo)

            // value parameters
            val parameters = trackSelector.parameters

            // value trackSelectionDialog
            val trackSelectionDialog = TrackSelectionDialog()

            trackSelectionDialog.init(
                    R.string.track_selection_title,                                         /* titleId= */
                    mappedTrackInfo,                                         /* initialParameters = */
                    parameters,                                              /* parameters */
                    false,                       /* allowAdaptiveSelections */
                    false,                        /* allowMultipleOverrides */
                    onClickListener = { _: DialogInterface?, _: Int ->       /* onClickListener */

                        // value builder
                        val builder = parameters.buildUpon()

                        // for looping dari 0 sampai mappedTrackInfo.rendererCount
                        for (i in 0 until mappedTrackInfo.rendererCount) {
                            builder
                                    .clearSelectionOverrides(   /* rendererIndex= */i)
                                    .setRendererDisabled(   /* rendererIndex= */
                                            i,
                                            trackSelectionDialog.getIsDisabled( /* rendererIndex= */i)
                                    )
                            val overrides = trackSelectionDialog.getOverrides(  /* rendererIndex= */i)

                            if (overrides.isNotEmpty()) {
                                builder.setSelectionOverride(
                                        i, /* rendererIndex= */
                                        mappedTrackInfo.getTrackGroups( /* rendererIndex= */i),
                                        overrides[0]
                                )
                            }
                        }
                        trackSelector.setParameters(builder)
                    },
                    onDismissListener = onDismissListener   /* onDismissListener */
            )
            return trackSelectionDialog
        }
    }


    /**
     * 6. Membuat fungsi onCreateView
     * */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialogView = inflater.inflate(R.layout.track_selection_dialog, container, false)

        /** 7. Membuat viewPager */
        val viewPager: ViewPager = dialogView.findViewById(R.id.track_selection_dialog_view_pager)

        /** 8. Membuat FragmentAdapter */
        viewPager.adapter = FragmentAdapter(childFragmentManager)

        /** 9. Membuat tabLayout */
        val tabLayout: TabLayout = dialogView.findViewById(R.id.track_selection_dialog_tab_layout)
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.visibility = if (tabFragments.size() > 1) View.VISIBLE else View.GONE

        /** 10. Membuat value, dan setOnclickListener okButton, dan cancelButton */
        val okButton = dialogView.findViewById<Button>(R.id.track_selection_dialog_ok_button)
        val cancelButton = dialogView.findViewById<Button>(R.id.track_selection_dialog_cancel_button)

        okButton.setOnClickListener {
            onClickListener!!.onClick(dialog, DialogInterface.BUTTON_POSITIVE)
            dismiss()
        }

        cancelButton.setOnClickListener {
            dismiss()
        }
        return dialogView
    }


    /**
     * 11. Membuat fungsi [getTrackTypeString] & [showTabForRenderer]
     * */
    private fun getTrackTypeString(resources: Resources, trackType: Int): String {
        return when (trackType) {
            C.TRACK_TYPE_VIDEO -> resources.getString(R.string.exo_track_selection_title_video)
            C.TRACK_TYPE_AUDIO -> resources.getString(R.string.exo_track_selection_title_audio)
            C.TRACK_TYPE_TEXT -> resources.getString(R.string.exo_track_selection_title_text)
            else -> throw IllegalArgumentException()
        }
    }


    /**
     * Fragment Adapter
     * */
    private inner class FragmentAdapter(fragmentManager: FragmentManager?) :
            FragmentPagerAdapter(fragmentManager!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("getPageTitle", getTrackTypeString(resources, tabTrackTypes[position]))
            return getTrackTypeString(resources, tabTrackTypes[position])
        }

        override fun getCount(): Int {
            return tabFragments.size()
        }

        override fun getItem(position: Int): Fragment {
            return tabFragments.valueAt(position)
        }
    }


    /**
     * 13. Membuat fungsi [getIsDisabled]
     *
     * Mengembalikan nilai disabled
     * @param rendererIndex Renderer index.
     * @mengembalikan nilai jika renderer bernilai disabled
     */
    fun getIsDisabled(rendererIndex: Int): Boolean {
        val rendererView = tabFragments[rendererIndex]
        return rendererView != null && rendererView.isDisabled
    }


    /**
     * 14. Membuat fungsi [getOverrides]
     *
     * Returns the list of selected track selection overrides for the specified renderer. There will
     * be at most one override for each track group.
     *
     * @param rendererIndex Renderer index.
     * @Mengembalikan nilai list dari track selection
     */
    fun getOverrides(rendererIndex: Int): List<SelectionOverride> {
        val rendererView = tabFragments[rendererIndex]

        Log.d("rendererView", "$rendererView")
        return if (rendererView == null) emptyList() else rendererView.overrides!!
    }


    /**
     * 16. Membuat fungsi [onCreateDialog]
     * */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // We need to own the view to let tab layout work correctly on all API levels. We can't use
        // AlertDialog because it owns the view itself, so we use AppCompatDialog instead, themed using
        // the AlertDialog theme overlay with force-enabled title.
        val dialog = AppCompatDialog(activity, R.style.TrackSelectionDialogThemeOverlay)
        dialog.setTitle(titleId)
        return dialog
    }


    /**
     * 17. Membuat fungsi [onDismiss]
     * */
    override fun onDismiss(dialog: DialogInterface) {
        onDismissListener!!.onDismiss(dialog)
        super.onDismiss(dialog)
    }


    /**
     * init constructor
     * */
    init {
        // Retain instance across activity re-creation to prevent losing access to init data.
        retainInstance = true
    }
}