package com.drt.rctiplus.models

public class Message {

    constructor() //empty for firebase

    /** deklarasi constructor messageText dengan type String */
    constructor(messageText: String) {
        /** Declaration variable for message text, type string */
        text = messageText
    }

    /** deklarasi variable text type String dengan nilai null */
    var text: String? = null

    /** deklarasi variable userName type String dengan nilai "Username" */
    var userName: String? = "Username"

    /** deklarasi value imgUri type String dengan nilai "https://www.shareicon.net/data/512x512/2016/05/24/770117_people_512x512.png" */
    val imgUri: String = "https://www.shareicon.net/data/512x512/2016/05/24/770117_people_512x512.png"

    /** deklarasi variable timestamp type Long dengan nilai System.currentTimeMillis() */
    var timestamp: Long = System.currentTimeMillis()
}