package com.drt.rctiplus.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.drt.rctiplus.R
import com.drt.rctiplus.models.Message
import kotlinx.android.synthetic.main.message_item.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * MessageAdapter Class
 *
 * Adapter dipanggil RecyclerView untuk membuat item baru dalam bentuk ViewHolder,
 * adapter jugak mengumpulkan atau menggabungkan item dengan data dan return informasi mengenai tentang data
 * seperti banyaknya item di dalam source data, Adapter meminta kita untuk meng override 3 method,
 * onCreateViewHolder(), onBindViewHolder(), dan getItemCount()
 **/
class MessageAdapter(private val messages: List<Message>, val itemClick: (Message) -> Unit) :
        RecyclerView.Adapter<MessageAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    class ViewHolder(view: View, val itemClick: (Message) -> Unit) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SimpleDateFormat")
                /*fungsi bind message dengan type Message*/
        fun bind(message: Message) {
            val timeNow = System.currentTimeMillis();
            val simpleDateFormat: SimpleDateFormat = SimpleDateFormat("hh:mm:ss")
            val date: Date = Date(message.timestamp)
            val time = simpleDateFormat.format(date)
            val timeMessage: Long = message.timestamp

            /** calculate for timeMinuteAgo and timeSecondAgo
             * fun kalkulasi untuk time ago */
            val timeDayAgo: Long = (timeNow - timeMessage) / 6000000
            val timeHoursAgo: Long = (timeNow - timeMessage) / 600000
            val timeMinuteAgo: Long = (timeNow - timeMessage) / 60000
            val timeSecondAgo: Long = (timeNow - timeMessage) / 6000
            var timeAgoVarString: String = ""

            /** conditions for var string from 1 day ago to now */
            /** kondisi untuk string 1 week ago sampai now */
            when {

                /** 2 month ago condition */
                timeHoursAgo > 8064 -> {
                    println("2 month ago")
                    timeAgoVarString = "2 month ago"
                }

                /** 1 month ago condition */
                timeHoursAgo > 4032 -> {
                    println("1 month ago")
                    timeAgoVarString = "1 month ago"
                }

                /** 3 weeks ago condition */
                timeHoursAgo > 3024 -> {
                    println("3 week ago")
                    timeAgoVarString = "3 weeks ago"
                }

                /** 2 weeks ago condition */
                timeHoursAgo > 2016 -> {
                    println("2 week ago")
                    timeAgoVarString = "2 weeks ago"
                }

                /** 1 week ago condition */
                timeHoursAgo > 1008 -> {
                    println("1 week ago")
                    timeAgoVarString = "1 week ago"
                }

                /** 3 days ago condition */
                timeHoursAgo > 432 -> {
                    println("3 days ago")
                    timeAgoVarString = "3 days ago"
                }

                /** 2 days ago condition */
                timeHoursAgo > 288 -> {
                    println("2 day ago")
                    timeAgoVarString = "2 days ago"
                }

                /** 1 day ago condition */
                timeHoursAgo > 144 -> {
                    println("1 day ago")
                    timeAgoVarString = "1 day ago"
                }

                /** 12 hours ago condition */
                timeHoursAgo > 72 -> {
                    println("12 hours ago")
                    timeAgoVarString = "12 hours ago"
                }

                /** 5 hours ago condition */
                timeHoursAgo > 30 -> {
                    println("5 hour ago")
                    timeAgoVarString = "5 hours ago"
                }

                /** 4 hours ago condition */
                timeHoursAgo > 24 -> {
                    println("4 hour ago")
                    timeAgoVarString = "4 hours ago"
                }

                /** 3 hours ago condition */
                timeHoursAgo > 18 -> {
                    println("3 hour ago")
                    timeAgoVarString = "3 hours ago"
                }

                /** 2 hours ago condition */
                timeHoursAgo > 12 -> {
                    println("2 hours ago")
                    timeAgoVarString = "2 hours ago"
                }

                /** 1 hour ago condition */
                timeHoursAgo > 6 -> {
                    println("an hour ago")
                    timeAgoVarString = "an hour ago"
                }

                /** 10 minutes ago condition */
                timeMinuteAgo > 10 -> {
                    println("10 minutes ago")
                    timeAgoVarString = "10 minutes ago"
                }

                /** 5 minutes ago condition */
                timeMinuteAgo > 5 -> {
                    println("5 minutes ago")
                    timeAgoVarString = "5 minutes ago"
                }
                /** 1 minute ago condition */
                timeMinuteAgo > 2 -> {
                    println("1 minute ago")
                    timeAgoVarString = "1 minute ago"
                }
                /** x seconds ago condition */
                timeSecondAgo > 1 -> {
                    println("$timeSecondAgo")
                    timeAgoVarString = "$timeSecondAgo seconds ago"
                }
                /** now condition */
                else -> {
                    println("now")
                    timeAgoVarString = "now"
                }
            }

            /** with
             * Memanggil fungsi blok message untuk membuat nilai dari message */
            with(message) {
//                itemView.messageUserName.text = message.userName.toString()
                itemView.setOnClickListener {
                    itemClick(this)
                }
            }
            itemView.messageTime.text = timeAgoVarString
            itemView.messageText.text = "${message.text}"
        }
    }
}